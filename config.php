<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/mysite/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/mysite/');

// DIR
define('DIR_APPLICATION', '/var/www/html/mysite/catalog/');
define('DIR_SYSTEM', '/var/www/html/mysite/system/');
define('DIR_LANGUAGE', '/var/www/html/mysite/catalog/language/');
define('DIR_TEMPLATE', '/var/www/html/mysite/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/html/mysite/system/config/');
define('DIR_IMAGE', '/var/www/html/mysite/image/');
define('DIR_CACHE', '/var/www/html/mysite/system/cache/');
define('DIR_DOWNLOAD', '/var/www/html/mysite/system/download/');
define('DIR_UPLOAD', '/var/www/html/mysite/system/upload/');
define('DIR_MODIFICATION', '/var/www/html/mysite/system/modification/');
define('DIR_LOGS', '/var/www/html/mysite/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'a');
define('DB_DATABASE', 'mysite');
define('DB_PREFIX', 'oc_');
