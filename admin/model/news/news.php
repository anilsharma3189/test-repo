<?php
class ModelNewsNews extends Model {
    
    function getNews() {
        $sql = "SELECT * FROM " . DB_PREFIX . "news";
        $query = $this->db->query($sql);

        return $query->rows;
    }
}