<?php
class ControllerNewsNews extends Controller {
    private $error = array();
    
    public function index() {
        $this->load->language('news/news');
        $this->document->setTitle($this->language->get('heading_title'));
        
        $this->load->model('news/news');
        
        $news = array();
        $news = $this->model_news_news->getNews();
        
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('news/news.tpl', $data));
    }
}

